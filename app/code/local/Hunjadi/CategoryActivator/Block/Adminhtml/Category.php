<?php
class Hunjadi_CategoryActivator_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Template
{
    public function getUpdates()
    {
        return Mage::getModel('categoryactivator/category')->getCollection();
    }
}