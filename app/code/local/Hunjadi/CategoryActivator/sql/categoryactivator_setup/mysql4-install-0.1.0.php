<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('categoryactivator/category')}` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `category_id` int(11) NOT NULL,
	  `is_active` tinyint(1) NOT NULL,
	  `set_active` tinyint(1) NOT NULL,
	  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `category_id` (`category_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();