<?php
class Hunjadi_CategoryActivator_Model_Category extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('categoryactivator/category');
    }
}