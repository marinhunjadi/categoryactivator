<?php
class Hunjadi_CategoryActivator_Model_Cron {

	const XML_PATH_LOG_FILE = 'categoryactivator/categoryactivator_group/log_file';
	const XML_PATH_APPROVAL_REQUIRED = 'categoryactivator/categoryactivator_group/approval_required';
	
	protected $categoryArray = array();

	private function getSubcategories($category)
    {
		$children = Mage::getModel('catalog/category')->getCollection()->addAttributeToFilter('parent_id', $category->getId());

		foreach ($children as $child) {
			if($this->categoryArray[$child->getId()]['child_is_active'] || $this->categoryArray[$child->getId()]['has_active_simple_products'] || $this->categoryArray[$child->getId()]['has_active_grouped_simple_products']){
				return true;
			}
			$this->getSubcategories($child);
		}
		
		return false;
	}
	
	public function categoryActivatorTester()
    {
		$filePath1 = Mage::getBaseDir('var').DS.'locks'.DS.'category_activator_tester.lock';
		$filePath2 = Mage::getBaseDir('var').DS.'locks'.DS.'category_activator_updater.lock';
		if(is_file($filePath2)){
			return;
		}		
		if(!is_file($filePath1)){
			fopen($filePath1, "w");
		}

		$collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter('level', array('gt' => 1))->setOrder('level', 'DESC');
		
		$disabledGroupedProducts = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToFilter('type_id', 'grouped')
			->addAttributeToFilter('status', '2');
			
		foreach($collection as $category){
			$this->categoryArray[$category->getId()]['id'] = $category->getId(); 
			$this->categoryArray[$category->getId()]['name'] = $category->getName();
			$this->categoryArray[$category->getId()]['is_active'] = $category->getIsActive();

			$this->categoryArray[$category->getId()]['child_is_active'] = '0';
			$this->categoryArray[$category->getId()]['has_active_simple_products'] = '0';
			$this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] = '0';
			
			if($this->getSubcategories($category)){
                $this->categoryArray[$category->getId()]['child_is_active'] = '1';
			}

			if($this->categoryArray[$category->getId()]['child_is_active'] == '0'){
				$simpleProducts = $category->getProductCollection()
					->addCategoryFilter($category)
					->addAttributeToFilter('type_id', 'simple')
					->addAttributeToFilter('status', '1');
					
				$this->categoryArray[$category->getId()]['has_active_simple_products'] = count($simpleProducts) ? '1' : '0';
			}
			
			if($this->categoryArray[$category->getId()]['child_is_active'] == '0' && $this->categoryArray[$category->getId()]['has_active_simple_products'] == '0'){
				$groupedProducts = $category->getProductCollection()
					->addAttributeToFilter('type_id', 'grouped')
					->addAttributeToFilter('status', '1');
					
				$this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] = count($groupedProducts) ? '1' : '0';
				
				if($this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] == '0'){
				$category = Mage::getModel('catalog/category')->load($category->getId());
					/*$disabledGroupedProducts = $category->getProductCollection()
						->addAttributeToFilter('type_id', 'grouped')
						->addAttributeToFilter('status', '2');*/			
					foreach ($disabledGroupedProducts as $product) {
						if(!in_array($category->getId(), $product->getCategoryIds())) continue;
						if($this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] == '1'){
							break;				
						}
						
						$associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
						foreach($associatedProducts as $associatedProduct){
							if($associatedProduct->getStatus() == '1'){
								$this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] = '1';
								break;
							}
						}
					}
				}
			}			

			$setActive = $this->categoryArray[$category->getId()]['child_is_active'] || $this->categoryArray[$category->getId()]['has_active_simple_products'] || $this->categoryArray[$category->getId()]['has_active_grouped_simple_products'] ? '1' : '0';
			if($category->getIsActive() != $setActive){
				$categoryActivator = Mage::getModel('categoryactivator/category')
					->getCollection()
					->addFieldToFilter('category_id', $category->getId())
					->getFirstItem();
					
				if($categoryActivator->getId()){
					$categoryActivator->setIsActive($category->getIsActive());
					$categoryActivator->setSetActive($setActive);
					$categoryActivator->setUpdatedAt(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
					$categoryActivator->save();
				} else {
					$categoryActivator = Mage::getModel('categoryactivator/category');
					$categoryActivator->setCategoryId($category->getId());
					$categoryActivator->setIsActive($category->getIsActive());
					$categoryActivator->setSetActive($setActive);
					$categoryActivator->setUpdatedAt(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
					$categoryActivator->save();
				}
			}
		}
		
		if(is_file($filePath1)){
			unlink($filePath1);
		}
	}
	
	public function categoryActivatorUpdater()
    {
		$filePath1 = Mage::getBaseDir('var').DS.'locks'.DS.'category_activator_tester.lock';
		$filePath2 = Mage::getBaseDir('var').DS.'locks'.DS.'category_activator_updater.lock';
		if(is_file($filePath1)){
			return;
		}		
		if(!is_file($filePath2)){
			fopen($filePath2, "w");
		}
		
		if(Mage::getStoreConfig(self::XML_PATH_APPROVAL_REQUIRED)){
			$run = Mage::app()->getRequest()->getParam('approved');
			if(is_null($run))
				return;
		}
		$currentStore = Mage::app()->getStore()->getId();
		Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
		
		$categories = Mage::getModel('catalog/category')
			->getCollection()
			->addAttributeToSelect('name')
			->addAttributeToSelect('is_active');
			
		$useLogFile = (bool)Mage::getStoreConfig(self::XML_PATH_LOG_FILE);
		
		foreach($categories as $category){
			$categoryActivator = Mage::getModel('categoryactivator/category')
				->getCollection()
				->addFieldToFilter('category_id', $category->getId())
				->getFirstItem();
			if($categoryActivator->getId() && $category->getIsActive() != $categoryActivator->getSetActive()){				
				$category->setIsActive($categoryActivator->getSetActive());
				$category->save();
				if($useLogFile){
					$status = $categoryActivator->getSetActive() == '1' ? ' enabled' : ' disabled';
					Mage::log($category->getId().': '.$category->getName().$status, null, 'categoryactivator.log');
				}
			}
		}
		
		Mage::app()->getStore()->setId($currentStore);
		
		$this->_clearTable();
		
		if(is_file($filePath2)){
			unlink($filePath2);
		}
	}
	
	private function _clearTable()
    {
		$collection = Mage::getModel('categoryactivator/category')->getCollection();
		foreach ($collection as $item) {
			$item->delete();
		}	
	}	
}