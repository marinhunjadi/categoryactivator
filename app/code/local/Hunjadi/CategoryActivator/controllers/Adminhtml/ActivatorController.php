<?php
class Hunjadi_CategoryActivator_Adminhtml_ActivatorController extends Mage_Adminhtml_Controller_Action
{
	const XML_PATH_APPROVAL_REQUIRED = 'categoryactivator/categoryactivator_group/approval_required';
	
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('categoryactivator_menu')
            ->_title($this->__('Index Action'));
			
		$this->_initLayoutMessages('core/session');
 
        $block = $this->getLayout()
        ->createBlock('categoryactivator/adminhtml_category', 'categoryactivator_category')
        ->setTemplate('categoryactivator/category.phtml');

        $this->_addContent($block);
 
        $this->renderLayout();
    }
	
	private function _clearTable()
    {
		$collection = Mage::getModel('categoryactivator/category')->getCollection();
		foreach ($collection as $item) {
			$item->delete();
		}	
	}
	
    public function updateAction()
    {
		if(!Mage::getStoreConfig(self::XML_PATH_APPROVAL_REQUIRED)) $this->_redirect('*/*/index'); 
		$session = Mage::getSingleton('core/session');
		try {
			Mage::getModel('categoryactivator/cron')->categoryActivatorUpdater();
			//$this->_clearTable();
			$session->addSuccess(Mage::helper('categoryactivator')->__('Your request has been successfully concluded.'));
			$this->_redirect('*/*/index');
		} catch (Exception $e) {
			$session->addError(Mage::helper('categoryactivator')->__('There was some error processing your request.'));
			$this->_redirect('*/*/index');
		}
    }
	
    public function cancelAction()
    {
		if(!Mage::getStoreConfig(self::XML_PATH_APPROVAL_REQUIRED)) $this->_redirect('*/*/index');
		$session = Mage::getSingleton('core/session');
		try {
			$this->_clearTable();		
			$session->addSuccess(Mage::helper('categoryactivator')->__('Your request has been successfully concluded.'));
			$this->_redirect('*/*/index');
		} catch (Exception $e) {
			$session->addError(Mage::helper('categoryactivator')->__('There was some error processing your request.'));
			$this->_redirect('*/*/index');
		}
    }
}