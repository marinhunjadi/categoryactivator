<?php
class Hunjadi_CategoryActivator_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction()
    {
    	Mage::getModel('categoryactivator/cron')->categoryActivatorTester();
		Mage::getModel('categoryactivator/cron')->categoryActivatorUpdater();
    }
	
}